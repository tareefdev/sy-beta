import React, { useContext } from "react";
import PropTypes from "prop-types";
import { Layout, GatsbyGramModal } from "mnemonic-www";
import { ModalRoutingContext } from "gatsby-plugin-modal-routing";
import { LocaleContext } from "../context/locale-context";

let windowWidth;

const UnitLayout = ({ location, isModal = false, children }) => {
  const locale = useContext(LocaleContext);
  if (!windowWidth && typeof window !== `undefined`) {
    windowWidth = window.innerWidth;
  }
  if (isModal && windowWidth > 750) {
    isModal = true;
  }
  //  if (isModal && GatsbyGramModal) {
  return (
    <ModalRoutingContext.Consumer>
      {({ modal, closeTo }) => (
        <div>
          {modal ? (
            <GatsbyGramModal isOpen={true} location={location}>
              {children}
            </GatsbyGramModal>
          ) : (
            <div
              css={{
                background: `rgba(0,0,0,0.03)`,
                minHeight: `100vh`
              }}
            ></div>
          )}
        </div>
      )}
    </ModalRoutingContext.Consumer>
  );
  //  }

  return <Layout locale={locale}>{children}</Layout>;
};

export default UnitLayout;
