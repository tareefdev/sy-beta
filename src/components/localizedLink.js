import React, { useContext } from "react";
import { Link } from "gatsby";
import { LocaleContext } from "../context/locale-context";
import locales from "../../config/i18n";

// Use the globally available context to choose the right path
function LocalizedLink({ to, ...props }) {
  const locale = useContext(LocaleContext);
  const isIndex = to === `/`;
  const path = `${locales[locale].path}/${isIndex ? `` : `${to}/`}`;

  return <Link {...props} to={path} />;
}

export default LocalizedLink;
