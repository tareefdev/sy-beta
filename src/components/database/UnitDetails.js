import React from "react";
import { graphql } from "gatsby";
import { css } from "@emotion/core";
import { FaAngleDown, FaCheck } from "react-icons/fa";
import useTranslations from "../hooks/useTranslations";
import { rhythm, scale } from "../../utils/typography";
import { colors } from "../../style/theme";

function UnitDetails({ unit, locale }) {
  const tr = useTranslations();
  const { id, link, title, incident_date_time, href, summary, location } = unit;
  const breakpoints = [576, 768, 992, 1200];
  const mq = breakpoints.map(bp => `@media (max-width: ${bp}px)`);
  return (
    <div
      css={{
        display: "grid",
        gridTemplateColumns: "2fr 1fr",
        gridColumnGap: rhythm(3),
        backgroundColor: colors.light,
        border: `3px solid ${colors.dark}`,
        padding: `${rhythm(3)} ${rhythm(5)}`,
        [mq[0]]: {
          display: "block",
          padding: rhythm(1)
        }
      }}
    >
      <div>
        <h2
          css={{
            marginTop: 0,
            [mq[0]]: {
              ...scale(0.6)
            }
          }}
        >
          {title}
        </h2>
        <small css={{ display: "block", opacity: 0.6 }}>
          <FaCheck />
          Verified Observation: 0326cc4
        </small>
        <small
          css={{
            backgroundColor: "#C20A0A",
            color: colors.light,
            fontWeight: "bold",
            padding: rhythm(0.2),
            paddingBottom: 0
          }}
        >
          {tr("Warning: this video may contain graphic content")}
        </small>
        <video
          controls
          width="100%"
          css={{
            marginTop: rhythm(1),
            marginBottom: rhythm(1)
          }}
        >
          <source src={link} type="video/mp4"></source>
        </video>
        <a href={href}>
          {tr("Download file")} <FaAngleDown />
        </a>
        <dl
          css={{
            marginTop: rhythm(2)
          }}
        >
          <dt>{tr("Online Link")}</dt>
          <dd>
            <a href={link}>{link}</a>
          </dd>
          <dt>{tr("Meta")}</dt>
          <dd>String here.</dd>
        </dl>
      </div>

      <div>
        <dl>
          <dt>{tr("Incident")}:</dt>
          <dd>empty</dd>
          <dt>{tr("Summary")}</dt>
          <dd>{summary}</dd>
          <dt>{tr("Date of occurency")}</dt>
          <dd>{incident_date_time}</dd>
          <dt>{tr("Locaiton")} </dt>
          <dd>{location.name}</dd>
          <dt>{tr("Precise Location")}:</dt>
          <dd>
            {location.lat} / {location.lon}
          </dd>
          <dt>{tr("Collection")}</dt>
          <dt>collection name</dt>
          <dt>{tr("Type of Violation")}:</dt>
          <dd>empty</dd>
        </dl>
      </div>
    </div>
  );
}

export default UnitDetails;

export const unitDetailFragment = graphql`
  fragment UnitDetails_detail on UnitsJson {
    id
    link
    title
    incident_date_time
    href
    summary
    location {
      name
      lat
      lon
    }
  }
`;
