import React, { useContext, useState, useEffect } from "react";
import PropTypes from "prop-types";
import { graphql } from "gatsby";
import { FaAngleLeft, FaCircle } from "react-icons/fa";
import { rhythm } from "../utils/typography";
import LocalizedLink from "../components/localizedLink";
import { LocaleContext } from "../context/locale-context";
import { Layout, PostHeader, charts, hooks } from "mnemonic-www";
import LeafletMap from "../components/database/map";
import { colors } from "../style/theme";
import useWindowSize from "../components/hooks/useWindowSize";
import useTranslations from "../components/hooks/useTranslations";
import { API_URL } from "../../env";

const { Timeline } = charts;
const { useDataApi } = hooks;

function IncidentCard({ incident }) {
  const {
    title,
    incident_date_time: { date },
    location: { name }
  } = incident;
  return (
    <div
      css={{
        backgroundColor: colors.light,
        marginBottom: rhythm(1),
        padding: `${rhythm(1)} ${rhythm(1.2)}`
      }}
    >
      <div
        css={{
          display: "flex",
          justifyContent: "space-between"
        }}
      >
        <small>283995a</small>
        <small>30 Videos</small>
      </div>
      <h3
        css={{
          marginTop: rhythm(0.4)
        }}
      >
        {title}
      </h3>
      <div>
        <span>{date}</span>
        <span>{name}</span>
      </div>
    </div>
  );
}

function ListIncidents({ incidents, isLoading, tr }) {
  return (
    <div>
      {isLoading ? (
        <div>{tr("Loading")} ...</div>
      ) : (
        incidents.map(incident => (
          <IncidentCard incident={incident} key={incident.id} />
        ))
      )}
    </div>
  );
}

function ChemicalCollection({ data: collection }) {
  const locale = useContext(LocaleContext);
  const [{ data, isLoading, isError }, doFetch] = useDataApi(
    `${API_URL}/collections/?collection=chemical&lang=${locale}`,
    []
  );
  const tr = useTranslations();
  const breakpoints = [576, 768, 992, 1200];
  const mq = breakpoints.map(bp => `@media (max-width: ${bp}px)`);
  const [windowWidthSize, windowHeightSize] = useWindowSize();
  const [hoveredUnit, setHoveredUnit] = useState({});
  const [rangeValue, setRangeValue] = useState({ min: 5, max: 10 });
  const [isMobile, setIsMobile] = useState(false);
  useEffect(() => {
    windowWidthSize < 992 ? setIsMobile(true) : setIsMobile(false);
  }, [windowWidthSize]);
  return (
    <Layout locale={locale}>
      <PostHeader post={collection.mdx} locale={locale} />
      <div
        css={{
          backgroundColor: colors.bg,
          paddingTop: rhythm(2.5)
        }}
      >
        <div
          css={{
            maxWidth: "1200px",
            margin: "auto"
          }}
        >
          <LocalizedLink
            to={"collections/chemical-weapons"}
            css={{
              position: "relative",
              marginLeft: rhythm(0.5)
            }}
          >
            <span
              css={{
                paddingRight: rhythm(0.4),
                position: "absolute",
                top: rhythm(-0.1),
                left: rhythm(-0.8)
              }}
            >
              <FaAngleLeft />
            </span>
            <span css={{ fontWeight: "bold" }}> Back to Key Findings</span>
          </LocalizedLink>
          <h4
            css={{
              marginTop: rhythm(1)
            }}
          >
            <FaCircle
              css={{
                color: colors.primary,
                fontSize: ".8em"
              }}
            />
            Incidents Data
          </h4>
          <div
            css={{
              margin: "0 auto",
              marginTop: rhythm(1),
              marginBottom: rhythm(2)
            }}
          >
            <Timeline
              lang={"en"}
              incidents={data.data}
              width={isMobile ? 500 : 1200}
              height={200}
              isLoading={isLoading}
            />

            {/* <InputRange */}
            {/*   minValue={0} */}
            {/*   maxValue={20} */}
            {/*   step={1} */}
            {/*   value={rangeValue} */}
            {/*   onChange={value => setRangeValue(value)} */}
            {/* /> */}
          </div>

          <div
            css={{
              display: "grid",
              gridTemplateColumns: "repeat(2, minmax(20px, 1fr))",
              gridColumnGap: rhythm(1),
              maxWidth: "1200px",
              margin: "auto",
              [mq[0]]: {
                display: "block"
              }
            }}
          >
            <ListIncidents
              incidents={data.data}
              isLoading={isLoading}
              tr={tr}
            />
            <div
              css={{
                position: "sticky",
                top: "12vh",
                height: "100vh",
                boxSizing: "border-box"
              }}
            >
              <LeafletMap
                units={data.data}
                hoveredUnit={hoveredUnit}
                isLoading={isLoading}
              />
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

IncidentCard.propTypes = {
  incident: PropTypes.object
};

ChemicalCollection.propTypes = {
  data: PropTypes.object
};

ListIncidents.propTypes = {
  incidents: PropTypes.array,
  isLoading: PropTypes.bool,
  tr: PropTypes.func
};

export default ChemicalCollection;

export const pageQuery = graphql`
  query CollectionPageB {
    mdx(frontmatter: { title: { regex: "/Chemical Weapons Database/" } }) {
      id
      frontmatter {
        title
        level
        date
        image {
          childImageSharp {
            fluid(maxWidth: 800) {
              ...GatsbyImageSharpFluid
            }
          }
        }
        desc
      }
      fields {
        postType
        parentDir
      }
    }
  }
`;
