import React, { useContext } from "react";
import PropTypes from "prop-types";
import { graphql } from "gatsby";
import { investigations, PageHeader, SEO, Layout } from "mnemonic-www";
import { LocaleContext } from "../context/locale-context";
import useTranslations from "../components/hooks/useTranslations";

const { Datasets, Reports } = investigations;

function InvestigationsDatasets({ data }) {
  const reports = data.reports.edges;
  const datasets = data.datasets.edges;
  const locale = useContext(LocaleContext);
  const tr = useTranslations();
  const isRTL = locale === "ar";
  const status = `${reports.length} ${tr("Investigations")} || ${
    datasets.length
  } ${tr("Datasets")}`;

  return (
    <Layout className={locale} locale={locale}>
      <SEO
        title="All posts"
        keywords={[`blog`, `gatsby`, `javascript`, `react`]}
      />
      <PageHeader
        title="Investigations"
        desc="investigation-page desc"
        status={status}
        locale={locale}
      />
      <Datasets nodes={datasets} locale={locale} />
      <Reports nodes={reports} locale={locale} />
    </Layout>
  );
}

InvestigationsDatasets.propTypes = {
  data: PropTypes.object
};

export default InvestigationsDatasets;

export const pageQuery = graphql`
  query InvestigationsDatasets($locale: String!) {
    site {
      siteMetadata {
        title
      }
    }
    reports: allMdx(
      filter: {
        fields: { locale: { eq: $locale } }
        fileAbsolutePath: { regex: "/content/investigations/" }
        frontmatter: { level: { lte: 1 } }
      }
      sort: { fields: [frontmatter___date], order: DESC }
    ) {
      edges {
        node {
          fields {
            slug
            locale
          }
          frontmatter {
            title
            date
            desc
            dataset
            level
            image {
              childImageSharp {
                fixed {
                  ...GatsbyImageSharpFixed
                }
              }
            }
          }
        }
      }
    }
    datasets: allMdx(
      filter: {
        fields: { locale: { eq: $locale } }
        fileAbsolutePath: { regex: "/content/collections/" }
      }
      sort: { fields: [frontmatter___date], order: DESC }
    ) {
      edges {
        node {
          fields {
            slug
            locale
          }
          frontmatter {
            title
            desc
            level
            date
            image {
              childImageSharp {
                fixed {
                  ...GatsbyImageSharpFixed
                }
              }
            }
          }
        }
      }
    }
  }
`;
