import React, { useContext } from "react";
import PropTypes from "prop-types";
import { graphql } from "gatsby";
import styled from "@emotion/styled";
import { Layout, helper, MdxLink, PreviewCompatibleImage } from "mnemonic-www";
import LocalizedLink from "../components/localizedLink";
import { rhythm, scale } from "../utils/typography";
import { LocaleContext } from "../context/locale-context";
import { find } from "lodash";
import { MDXRenderer } from "gatsby-plugin-mdx";
import { colors } from "../style/theme";
import useTranslations from "../components/hooks/useTranslations";
import { mq } from "../utils/helper";

//const { mq } = helper;

const SidebarNote = styled.p({
  fontWeight: 500,
  maxWidth: rhythm(9.84),
  color: colors.primary
});

const SidebarButton = styled.a({
  border: `3px solid ${colors.dark}`,
  padding: "0.3rem 1rem 0.1rem 1rem",
  ":hover, :active": {
    color: colors.light,
    backgroundColor: colors.dark
  }
});

const BlackBar = () => (
  <div
    css={{
      borderBottom: `6px solid ${colors.dark}`,
      width: "70%",
      margin: `${rhythm(2)} 0`
    }}
  ></div>
);

function About({ data }) {
  const posts = data.aboutPosts.edges.map(p => p.node);
  const partnersImages = data.partnersImages.edges.map(p => p.node);
  const updatesPosts = posts.filter(p => p.frontmatter.title !== "index");
  const locale = useContext(LocaleContext);
  const isRTL = locale === "ar";
  const tr = useTranslations();
  const indexPost = find(posts, {
    frontmatter: {
      title: "index"
    }
  });

  return (
    <Layout locale={locale}>
      <div
        css={{
          maxWidth: rhythm(45),
          margin: `${rhythm(2)} auto`,
          paddingTop: rhythm(3),
          direction: isRTL ? "rtl" : "ltr"
        }}
      >
        <span
          css={{
            ...scale(1),
            textTransform: "uppercase",
            borderBottom: `6px solid ${colors.dark}`,
            [mq[0]]: {
              margin: rhythm(1)
            }
          }}
        >
          {tr("About")}
        </span>
        <div
          css={{
            display: "grid",
            gridTemplateColumns: "2fr 1fr",
            gridColumnGap: rhythm(3),
            direction: isRTL ? "rtl" : "ltr",
            [mq[0]]: {
              display: "block",
              margin: rhythm(1)
            }
          }}
        >
          <div>
            <MDXRenderer
              components={{
                a: MdxLink,
                img: PreviewCompatibleImage
              }}
            >
              {indexPost.body}
            </MDXRenderer>
            <BlackBar />
            <div>
              {updatesPosts.map(post => (
                <h4
                  css={{
                    ...scale(0.2),
                    marginTop: rhythm(0.8),
                    letterSpacing: "0.3px",
                    color: colors.primary,
                    fontWeight: 500
                  }}
                  key={post.id}
                >
                  <LocalizedLink
                    to={post.fields.slug}
                    css={{
                      display: "inline-block",
                      padding: "0.2rem 0.2rem 0 0.2rem",
                      ":hover, :active": {
                        backgroundColor: colors.dark,
                        color: colors.light
                      }
                    }}
                  >
                    {post.frontmatter.title}
                  </LocalizedLink>
                </h4>
              ))}
            </div>
          </div>
          <div>
            <h2
              css={{
                marginTop: rhythm(2)
              }}
            >
              {tr("Funding")}
            </h2>
            <SidebarNote>{tr("about-page support")}</SidebarNote>
            <SidebarButton
              href="https://www.patreon.com/syrianarchive"
              rel="noopener noreferrer"
              target="_blank"
            >
              {tr("Donate")}
            </SidebarButton>
            <BlackBar />
            <h2>{tr("Partners")}</h2>
            {partnersImages.map(image => (
              <div
                key={image.id}
                css={{
                  marginBottom: rhythm(2)
                }}
              >
                <PreviewCompatibleImage
                  imageInfo={{
                    image: image,
                    alt: "non"
                  }}
                  imageStyle={{
                    width: "50%",
                    height: "70%"
                  }}
                />
              </div>
            ))}
            <SidebarButton href="#">{tr("Support Us")}</SidebarButton>
            <BlackBar />
            <h2>{tr("Funders")}</h2>
            <SidebarNote
              css={{
                color: colors.dark
              }}
            >
              {tr("about-page funders")}
            </SidebarNote>
          </div>
        </div>
      </div>
    </Layout>
  );
}

About.propTypes = {
  data: PropTypes.object
};

export default About;

export const pageQuery = graphql`
  query aboutPage($locale: String!) {
    aboutPosts: allMdx(
      filter: {
        fields: { locale: { eq: $locale } }
        fileAbsolutePath: { regex: "/content/about/" }
      }
      sort: { fields: [frontmatter___date], order: DESC }
    ) {
      edges {
        node {
          frontmatter {
            title
            date
            desc
            image {
              childImageSharp {
                fixed {
                  ...GatsbyImageSharpFixed
                }
              }
            }
          }
          body
          fields {
            slug
          }
        }
      }
    }
    partnersImages: allFile(filter: { name: { regex: "/sy-partners/" } }) {
      edges {
        node {
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
`;
