import React, { useContext } from "react";
import PropTypes from "prop-types";
import { graphql } from "gatsby";
import { set } from "lodash";
import { homepage, Layout, SEO } from "mnemonic-www";
import { LocaleContext } from "../context/locale-context";

const {
  Slider,
  Workflow,
  About,
  WhoWeAre,
  Supportus,
  PressCoverage
} = homepage;

const workflowItems = [
  { label: "collect", num: "5,024", desc: "Sources", id: "0" },
  { label: "preserve", num: "3,578,591", desc: "Videos", id: "1" },
  { label: "process", num: "650,000", desc: "Video Analysed", id: "2" },
  { label: "verify", num: "8,249", desc: "Videos", id: "3" },
  { label: "Investigate", num: "2,069", desc: "Incidents", id: "4" }
];

function Index({ data }) {
  const locale = useContext(LocaleContext);
  const latestInvestigation = data.latestInvestigation.edges[0].node;
  const latestDatabase = data.latestDatabase.edges[0].node;
  const latestTech = data.latestTechAdvocacy.edges[0].node;
  const latestPressUpdates = data.latestPressUpdates.edges.map(p => p.node);
  set(latestDatabase, "type", "database");
  set(latestTech, "type", "tech advocacy");
  set(latestInvestigation, "type", "investigation");
  const sliderItems = [latestInvestigation, latestDatabase, latestTech];

  return (
    <Layout locale={locale}>
      <SEO title="Index Page" />
      <Slider items={sliderItems} locale={locale} />
      <About locale={locale} />
      <Workflow items={workflowItems} locale={locale} />
      <WhoWeAre locale={locale} />
      <Supportus locale={locale} />
      <PressCoverage latestPressUpdates={latestPressUpdates} locale={locale} />
    </Layout>
  );
}

Index.propTypes = {
  data: PropTypes.object
};

export default Index;

export const pageQuery = graphql`
  query IndexPartials($locale: String!) {
    partials: allMdx(
      filter: {
        fileAbsolutePath: { regex: "/content/partials/" }
        frontmatter: { page: { eq: "index" } }
      }
    ) {
      edges {
        node {
          id
          frontmatter {
            name
          }
          body
        }
      }
    }

    latestPressUpdates: allPressJson(filter: { lang: { eq: $locale } }) {
      edges {
        node {
          id
          link
          title
          date
          image
        }
      }
    }

    latestTechAdvocacy: allMdx(
      filter: {
        fields: { locale: { eq: $locale } }
        fileAbsolutePath: { regex: "/content/tech-advocacy/" }
      }
      sort: { fields: [frontmatter___date], order: DESC }
      limit: 3
    ) {
      edges {
        node {
          id
          fields {
            slug
          }
          frontmatter {
            title
            desc
            date
            image {
              childImageSharp {
                fluid(maxWidth: 2000) {
                  src
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
    latestInvestigation: allMdx(
      filter: {
        fields: { locale: { eq: $locale } }
        fileAbsolutePath: { regex: "/content/investigations/" }
        frontmatter: { level: { lte: 1 } }
      }
      sort: { fields: [frontmatter___date], order: DESC }
      limit: 1
    ) {
      edges {
        node {
          id
          fields {
            slug
          }
          frontmatter {
            title
            desc
            image {
              childImageSharp {
                fluid(maxWidth: 2000) {
                  src
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
    latestDatabase: allMdx(
      filter: {
        fields: { locale: { eq: $locale } }
        fileAbsolutePath: { regex: "/content/collections/" }
      }
      sort: { fields: [frontmatter___date], order: DESC }
      limit: 1
    ) {
      edges {
        node {
          id
          fields {
            slug
          }
          frontmatter {
            title
            desc
            image {
              childImageSharp {
                fluid(maxWidth: 2000) {
                  src
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`;
