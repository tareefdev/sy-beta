import React, { useContext } from "react";
import PropTypes from "prop-types";
import { graphql } from "gatsby";
import { Layout, SEO } from "mnemonic-www";
import useTranslations from "../components/hooks/useTranslations";
import { LocaleContext } from "../context/locale-context";

function NotFoundPage({ data }) {
  const siteTitle = data.site.siteMetadata.title;
  const locale = useContext(LocaleContext); // used in Graphql query
  const tr = useTranslations();

  return (
    <Layout title={siteTitle} locale={locale}>
      <SEO title="404: Not Found" />
      <h1>{tr("Not Found")}</h1>
      <p>{tr("notfound-page content")}</p>
    </Layout>
  );
}

NotFoundPage.propTypes = {
  data: PropTypes.object
};

export default NotFoundPage;

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`;
