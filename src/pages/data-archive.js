import React, { useState, useContext, useEffect } from "react";
import { Layout, dataArchive, PageHeader, helper, hooks } from "mnemonic-www";
import useTranslations from "../components/hooks/useTranslations";
import useWindowSize from "../components/hooks/useWindowSize";
import { LocaleContext } from "../context/locale-context";
import { rhythm } from "../utils/typography";
import { colors } from "../style/theme";
import { GoSettings } from "react-icons/go";
import { API_URL } from "../../env";
import { mq } from "../utils/helper";
const { SearchForm, ListData } = dataArchive;
const { useDataApi } = hooks;
//const { mq } = helper;

function DataArchivePage() {
  const locale = useContext(LocaleContext);
  const [{ data, isLoading, isError }, doFetch] = useDataApi(
    `${API_URL}/observations/?page=1&lang=${locale}`,
    []
  );
  const [windowWidthSize] = useWindowSize();
  const [isFilltersCollapsed, setIsFilltersCollapsed] = useState(true);

  useEffect(() => {
    windowWidthSize < 992
      ? setIsFilltersCollapsed(false)
      : setIsFilltersCollapsed(true);
  }, [windowWidthSize]);

  const isRTL = locale === "ar";
  const tr = useTranslations();
  return (
    <Layout locale={locale}>
      <PageHeader title="The Archive" desc="data-page desc" locale={locale} />
      <div
        css={{
          backgroundColor: colors.bg
        }}
      >
        <div
          css={{
            display: "grid",
            gridTemplateColumns: "1fr 2fr",
            maxWidth: rhythm(53),
            paddingTop: rhythm(2),
            margin: "0 auto",
            direction: isRTL ? "rtl" : "ltr",
            [mq[0]]: {
              display: "block",
              minWidth: rhythm(15)
            }
          }}
        >
          <div
            css={{
              maxWidth: "90%",
              margin: "0 auto"
            }}
          >
            <div
              css={{
                display: "none",
                [mq[1]]: {
                  display: "flex",
                  marginBottom: rhythm(1)
                }
              }}
            >
              <p css={{ width: "50%", direction: isRTL ? "rtl" : "ltr" }}>
                {tr("videos")}: 3,314,265 {tr("collected")} (5,743{" "}
                {tr("verified")})
              </p>
              <GoSettings
                onClick={() => setIsFilltersCollapsed(!isFilltersCollapsed)}
                css={{
                  cursor: "pointer",
                  fontSize: "130%",
                  marginLeft: isRTL ? 0 : "auto",
                  marginRight: isRTL ? "auto" : 0
                }}
              />
            </div>
            {isFilltersCollapsed && (
              <SearchForm
                submitAction={doFetch}
                currentPage={data.page}
                pageCount={data.pageCount}
                ApiURL={API_URL}
                locale={locale}
              />
            )}
          </div>
          {isError && <div>{tr("Something went wrong")} ...</div>}
          {isLoading ? (
            <div>{tr("Loading")} ...</div>
          ) : (
            <ListData units={data.data} locale={locale} />
          )}
        </div>
      </div>
    </Layout>
  );
}

export default DataArchivePage;
