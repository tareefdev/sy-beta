import React, { useContext } from "react";
import PropTypes from "prop-types";
import { Layout } from "mnemonic-www";
import { graphql } from "gatsby";
import { MDXRenderer } from "gatsby-plugin-mdx";
import { MDXProvider } from "@mdx-js/react";
import { rhythm, scale } from "../utils/typography";
import { colors } from "../style/theme";
import styled from "@emotion/styled";
import { LocaleContext } from "../context/locale-context";
import useTranslations from "../components/hooks/useTranslations";
import { mq } from "../utils/helper";

const H2 = styled.h2({
  position: "relative",
  marginTop: 0,
  marginBottom: rhythm(2.3),
  "&::before": {
    position: "absolute",
    top: 0,
    left: rhythm(-2.2),
    display: "block",
    width: "200px",
    height: "200px",
    content: "url('/assets/oval.svg')"
  }
});

function MethodsTools({ data }) {
  const mainText = data.mdx;
  const locale = useContext(LocaleContext);
  const tr = useTranslations();
  const isRTL = locale === "ar";
  return (
    <Layout locale={locale}>
      <div
        css={{
          maxWidth: rhythm(45),
          margin: "2rem auto 4rem auto",
          direction: isRTL ? "rtl" : "ltr",
          [mq[1]]: {
            margin: rhythm(2)
          }
        }}
      >
        <h1
          css={{
            ...scale(1),
            textTransform: "uppercase"
          }}
        >
          {tr("Tools and methods")}
        </h1>
        <div
          css={{
            borderBottom: `6px solid ${colors.dark}`,
            maxWidth: rhythm(24),
            marginBottom: rhythm(2)
          }}
        ></div>
        <div
          css={{
            borderLeft: "6px solid #FFA97F",
            paddingLeft: rhythm(1.5)
          }}
        >
          <MDXProvider
            components={{
              h2: H2
            }}
          >
            <MDXRenderer>{mainText.body}</MDXRenderer>
          </MDXProvider>
        </div>
      </div>
    </Layout>
  );
}

MethodsTools.propTypes = {
  data: PropTypes.object
};

export default MethodsTools;

export const pageQuery = graphql`
  query MethodsTools($locale: String!) {
    mdx(
      frontmatter: { title: { eq: "index of tools and methods" } }
      fields: { locale: { eq: $locale } }
    ) {
      body
    }
  }
`;
