import React, { useContext } from "react";
import PropTypes from "prop-types";
import { Layout, Card, MdxLink, PreviewCompatibleImage } from "mnemonic-www";
import LocalizedLink from "../components/localizedLink";
import { rhythm } from "../utils/typography";
import { colors } from "../style/theme";
import { graphql } from "gatsby";
import { LocaleContext } from "../context/locale-context";
import { MDXRenderer } from "gatsby-plugin-mdx";
import useTranslations from "../components/hooks/useTranslations";

function Intro({ tr, isRTL }) {
  return (
    <div>
      <div
        css={{
          paddingTop: rhythm(6),
          paddingBottom: rhythm(5),
          backgroundColor: colors.dark,
          backgroundImage:
            "linear-gradient(180deg, rgba(0,0,0,0.63) 0%, rgba(175,58,0,0.61) 100%)",
          color: colors.light,
          direction: isRTL ? "rtl" : "ltr"
        }}
      >
        <div
          css={{
            display: "flex",
            justifyContent: "space-between",
            maxWidth: "830px",
            margin: "auto"
          }}
        >
          <div css={{ textAlign: "center" }}>
            <h1 css={{ margin: 0 }}>350,357</h1>
            <span>{tr("videos restored")}</span>
          </div>
          <div css={{ maxWidth: "560px" }}>
            <p
              css={{
                opacity: 0.75,
                fontSize: "1.5rem",
                fontWeight: "bold",
                margin: 0
              }}
            >
              {tr("lost-and-found intro")}
            </p>
          </div>
        </div>

        <div
          css={{
            maxWidth: "1072px",
            margin: "6rem auto",
            "& video": { width: "100%" }
          }}
        >
          <video autoPlay loop muted playsinline>
            <source src="/assets/lost-and-found.webm" type="video/webm" />
          </video>
        </div>

        <div
          css={{
            display: "flex",
            justifyContent: "space-between",
            maxWidth: "700px",
            margin: "auto"
          }}
        >
          <p
            css={{ fontWeight: "bold", fontSize: "1.5rem", maxWidth: "480px" }}
          >
            {tr("lost-and-found in-touch")}
          </p>
          <div
            css={{
              alignSelf: "start",
              display: "flex",
              flexDirection: "column",
              alignItems: "center"
            }}
          >
            <a
              href="#"
              css={{
                border: `3px solid ${colors.light}`,
                color: colors.light,
                fontWeight: "bold",
                letterSpacing: "0.88px",
                textTransform: "uppercase",
                fontSize: "10.24px",
                padding: "0.4rem 0.8rem"
              }}
            >
              {tr("Get in touch")}
            </a>
            <small css={{ marginTop: "0.5rem" }}>info@syrianarchive.org</small>
          </div>
        </div>
      </div>
    </div>
  );
}

function WhatWeDo({ text, isRTL }) {
  return (
    <div
      css={{
        maxWidth: "950px",
        margin: "auto",
        lineHeight: "26px",
        direction: isRTL ? "rtl" : "ltr"
      }}
    >
      <MDXRenderer
        components={{
          a: MdxLink,
          img: PreviewCompatibleImage
        }}
      >
        {text.body}
      </MDXRenderer>
    </div>
  );
}

function MonthlyUpdates({ posts, tr, isRTL, locale }) {
  return (
    <div
      css={{
        maxWidth: "1200px",
        margin: `${rhythm(4)} auto`,
        direction: isRTL ? "rtl" : "ltr"
      }}
    >
      <h2>
        <LocalizedLink to="/tech-advocacy-archive">
          {tr("Monthly takedown updates")}
        </LocalizedLink>
      </h2>
      <div
        css={{
          display: "grid",
          gridTemplateColumns: "repeat(4, 1fr)",
          gridColumnGap: rhythm(2)
        }}
      >
        {posts.map(node => (
          <Card node={node} key={node.fields.slug} locale={locale} />
        ))}
      </div>
    </div>
  );
}

function LostandFound({ data }) {
  const posts = data.posts.edges.map(p => p.node);
  const updatesPosts = posts.filter(p => p.frontmatter.title !== "index");
  const indexPost = data.main;
  const locale = useContext(LocaleContext);
  const tr = useTranslations();
  const isRTL = locale === "ar";
  return (
    <Layout locale={locale}>
      <Intro tr={tr} isRTL={isRTL} />
      <WhatWeDo text={indexPost} isRTL={isRTL} />
      <MonthlyUpdates
        posts={updatesPosts}
        tr={tr}
        isRTL={isRTL}
        locale={locale}
      />
    </Layout>
  );
}

LostandFound.propTypes = {
  data: PropTypes.object
};

export default LostandFound;

export const pageQuery = graphql`
  query TechAdvocacy($locale: String!) {
    posts: allMdx(
      filter: {
        fields: { locale: { eq: $locale } }
        fileAbsolutePath: { regex: "/content/tech-advocacy/" }
      }
      sort: { fields: [frontmatter___date], order: DESC }
    ) {
      edges {
        node {
          frontmatter {
            title
            date
            desc
            image {
              childImageSharp {
                fixed {
                  ...GatsbyImageSharpFixed
                }
              }
            }
          }
          fields {
            slug
          }
          body
        }
      }
    }
    main: mdx(
      frontmatter: { title: { eq: "index" } }
      fileAbsolutePath: { regex: "/tech-advocacy/" }
      fields: { locale: { eq: $locale } }
    ) {
      body
    }
  }
`;
