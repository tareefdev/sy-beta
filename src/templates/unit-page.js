import React from "react";
import { graphql } from "gatsby";
import UnitDetails from "../components/database/UnitDetails";
//import { Database } from "mnemonic-www";
import UnitLayout from "../components/UnitLayout";

//const { UnitDetails } = Database;

function unitTemplate({ pageContext, data, location }) {
  const locale = pageContext.locale;
  let isModal = false;
  // We don't want to show the modal if a user navigates
  // directly to a unit so if this code is running on Gatsby's
  // initial render then we don't show the modal, otherwise we
  // do.
  if (
    typeof window !== "undefined" &&
    window.___MNEMONIC_INITIAL_RENDER_COMPLETE
  ) {
    isModal = true;
  }

  return (
    <UnitLayout location={location} isModal={isModal}>
      <UnitDetails unit={data.unitsJson} locale={locale} />
    </UnitLayout>
  );
}

export default unitTemplate;

export const pageQuery = graphql`
  query($id: String!) {
    unitsJson(id: { eq: $id }) {
      id
      ...UnitDetails_detail
    }
  }
`;
