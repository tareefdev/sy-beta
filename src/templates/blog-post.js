import React, { useContext, useState, useEffect } from "react";
import { graphql } from "gatsby";
import PropTypes from "prop-types";
import { MDXProvider } from "@mdx-js/react";
import { MDXRenderer } from "gatsby-plugin-mdx";
import { Layout, SEO, helper, PostHeader } from "mnemonic-www";
import MdxLink from "../components/mdxLink.js";
import useWindowSize from "../components/hooks/useWindowSize";
import styled from "@emotion/styled";
import LocalizedLink from "../components/localizedLink";
import { colors } from "../style/theme";
import { rhythm } from "../utils/typography";
import { LocaleContext } from "../context/locale-context";
import useTranslations from "../components/hooks/useTranslations";
import { mq } from "../utils/helper";

//const { useWindowSize } = hooks;
//const { mq } = helper;

function TableOfContents({ post, tr }) {
  const locale = useContext(LocaleContext);
  const isRTL = locale === "ar";
  const toc = post.tableOfContents.items;
  const isCollection = post.frontmatter.type === "database";
  const headOfToc = isCollection ? "Key findings" : "Table Of Content";
  if (toc === undefined) {
    return <div></div>;
  } else {
    return (
      <div>
        <ul
          css={{
            position: "sticky",
            top: rhythm(6),
            left: isRTL ? "auto" : rhythm(2),
            right: isRTL ? rhythm(3) : "auto",
            maxWidth: rhythm(8),
            marginBottom: rhythm(5),
            maxHeight: "calc(-10rem + 100vh);",
            overflowY: "auto",
            listStyle: "none",
            direction: isRTL ? "rtl" : "ltr"
          }}
        >
          <h4
            css={{
              margin: 0,
              marginBottom: rhythm(1)
            }}
          >
            {tr(headOfToc)}
          </h4>
          {toc.map(item => (
            <li
              key={item.url}
              css={{
                borderBottom: "1px solid #CACACA",
                padding: "0.5rem"
              }}
            >
              <a
                href={item.url}
                css={{
                  ":hover, :active": {
                    color: colors.primary
                  }
                }}
              >
                {item.title}
              </a>
            </li>
          ))}
          {isCollection && (
            <h4>
              <LocalizedLink to={"chemical-collection"}>
                {tr("Incidents Data")}
              </LocalizedLink>
            </h4>
          )}
        </ul>
      </div>
    );
  }
}

function BlogPostTemplate({ data }) {
  const siteTitle = data.site.siteMetadata.title;
  const post = data.mdx;
  const [windowWidthSize, windowHeightSize] = useWindowSize();
  const [isMobile, setIsMobile] = useState(false);
  const locale = useContext(LocaleContext);
  const isRTL = locale === "ar";
  const tr = useTranslations();

  useEffect(() => {
    windowWidthSize < 992 ? setIsMobile(true) : setIsMobile(false);
  }, [windowWidthSize]);

  useEffect(() => {
    // This is releated to the workaound in gatsby-browser.js file
    window.scrollTo(0, 0);
  }, []);

  const designSystem = {
    a: styled(MdxLink)({
      color: colors.primary,
      fontWeight: "bold"
    }),
    p: styled.p({
      color: colors.dark,
      lineHeight: "26px"
    }),
    h1: styled.h1({
      opacity: 0.7
    }),
    h2: styled.h2({
      opacity: 0.7
    }),
    h3: styled.h3({
      opacity: 0.7
    }),
    h4: styled.h4({
      opacity: 0.7
    }),
    h5: styled.h5({
      opacity: 0.7
    })
  };

  return (
    <Layout locale={locale}>
      <SEO
        title={post.frontmatter.title}
        description={post.frontmatter.description || post.excerpt}
      />

      <PostHeader post={data.mdx} locale={locale} />

      <div
        css={{
          display: "grid",
          gridTemplateColumns: "25% 50% 25%",
          paddingTop: rhythm(2),
          direction: isRTL ? "rtl" : "ltr",
          [mq[0]]: {
            display: "block",
            marginRight: rhythm(1.3),
            marginLeft: rhythm(1.3)
          }
        }}
      >
        {isMobile || <TableOfContents post={data.mdx} tr={tr} />}
        <div
          css={{
            maxWidth: rhythm(32.25),
            marginBottom: rhythm(4),
            justifySelf: "center",
            "ul:first-of-type": {
              border: "6px solid ${colors.dark}",
              boxShadow: "-5px 5px 12px 0 rgba(0,0,0,0.25)",
              padding: `${rhythm(1)} ${rhythm(2)}`,
              "& li": {
                listStyle: "none",
                margin: 0,
                "&::before": {
                  content: '"•"',
                  color: colors.primary,
                  display: "inline-block",
                  width: "0.8em",
                  fontSize: "2em"
                }
              },
              [mq[0]]: {
                paddingLeft: rhythm(1),
                paddingRight: rhythm(1),
                marginLeft: 0
              }
            }
          }}
        >
          <MDXProvider
            components={{
              a: designSystem.a,
              p: designSystem.p,
              h1: designSystem.h1,
              h2: designSystem.h2,
              h3: designSystem.h3,
              h4: designSystem.h4,
              h5: designSystem.h5
            }}
          >
            <MDXRenderer>{post.body}</MDXRenderer>
          </MDXProvider>
        </div>
        {isMobile || <div></div>}
      </div>
    </Layout>
  );
}

BlogPostTemplate.propTypes = {
  data: PropTypes.object
};

export default BlogPostTemplate;

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!, $locale: String!) {
    site {
      siteMetadata {
        title
      }
    }
    mdx(fields: { slug: { eq: $slug }, locale: { eq: $locale } }) {
      id
      excerpt(pruneLength: 160)
      tableOfContents
      body
      frontmatter {
        title
        level
        date
        type
        image {
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
        desc
      }
      fields {
        slug
        locale
        postType
        parentDir
      }
    }
  }
`;
