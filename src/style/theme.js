const colors = {
  primary: "#ff5400",
  light: "#ffffff",
  dark: "#000000",
  bg: "#f5f5f5"
};

export { colors };
