---
title: "index"
date: 2019-01-01
desc: "Amount of content preserved, made unavailable and restored"
image: /assets/takedowns02.jpg
level: 1
---

## Syrian Archive’s work on content taken down from social media platforms

Gaining physical access to be able to investigate and report on human rights violations in Syria is very limited and dangerous for independent journalists, international news agencies, UN investigation bodies and international human rights organisations. This is the main reason Syrian Archive and other documentation groups depend on verified user generated content to assist in criminal case building as well as human rights research. 

In the Syrian conflict, there are more hours of videos documenting the conflict than there have been hours in the conflict itself. Even now, eight years after the Syrian conflict began in 2011, more than 50 videos are uploaded to YouTube each day, making it an “accidental archive” that arguably allows anyone in the world to witness a conflict for the first time in history, practically in real time.

