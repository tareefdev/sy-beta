---
layout: tilecontent.html
title: index of tools and methods
image: /assets/technology/technologyyy.jpg
desc: Open Source Tools and Methods for Open Source Investigations
level: 1
---
Gaining physical access to be able to investigate and report on human rights violations in Syria is very limited and dangerous for independent journalists, international news agencies, UN investigation bodies and international human rights organisations. This is the main reason Syrian Archive and other documentation groups depend on verified user generated content to assist in criminal case building as well as human rights research.

Syrian Archive strives for transparency in its tools, findings, and methodologies, as well as in making sure that verified content is publicly available and accessible for journalists, human rights defenders, and lawyers for reporting, advocacy and accountability purposes.

One of the ways this is done is through releasing all software developed publicly in free and open-source formats. This is done both to ensure trust is built and maintained between the Syrian Archive and its partners and collaborators, as well as to allow software to be reused and customised by other groups outside of this project. Technical integration with existing open-source investigative tools ensure that work is not duplicated.

We work in collaboration with other archival groups as well as lawyers and journalists to develop our methodology, and with other technologists to develop the open source tools we are using.

The Syrian Archive employed its Digital Evidence Workflow, inspired by the [Electronic Discovery Reference Model developed by Duke University School of Law][1].

## Collection

Syrian Archive's collection process has three steps: 1) Establish a database of credible sources for digital content; 2) Establish a database of credible sources for verification; 3) Establish a standardised metadata schema.

### 1) Establish database of credible sources for content

Before any collection, archival, or verification of digital materials is possible, Syrian Archive first established a database of credible sources for visual content. Syrian Archive has worked to identify over 3.500 credible sources, a list consisting of individual journalists and field reporters, larger media houses (e.g. local and international news agencies), human rights organisations (e.g. Syria Institute for Justice), Syrian Civil Defense (White Helmets), and local field clinics and hospitals, and others. Many of the sources used by the Syrian Archive began publishing or providing visual content in late 2011-early 2012 and have also published work in other credible media outlets.

Our database collects data from a list of sources in a variety of different source types.  We acquire data and posts daily from these sources. Types of sources include social media channels (Twitter, Facebook, YouTube), submitted files (videos, pdf), and external and collaborator's data sets. Changes in these sources are tracked, meaning that all versions are saved.

Credibility was determined by analysing whether the source is familiar to the Syrian Archive or to its existing professional network of Syrian journalists, media activists, human rights groups and humanitarian workers; whether the source's content and reportage been reliable in the past. This is determined by evaluating how long the source has been reporting and how active they are. To determine where the source is based, social media channels are evaluated to determine whether videos uploaded are consistent and mostly from a specific location where the source is based, or if locations differ significantly. Channels are analysed to determine whether the video account uses a logo and whether this logo is consistently used across videos. Channels are additionally analysed for original content to determine whether the uploader aggregates videos from other news organisations or accounts, or whether the source appears to be the primary uploader.

### 2) Establish database of credible sources for verification

Syrian Archive established a database of credible sources for verification. These sources provide additional information used for verification of content originating on social media platforms or sent from sources directly. Those verifying content are made up of citizen journalists, human rights defenders and humanitarian workers based in Syria and abroad. To preserve data integrity, sources used for content did not comprise part of the database for verification.

### 3) Establish standardised metadata scheme

Syrian Archive recognised the need for a data ontology, or standardised metadata scheme, for organising content. We recognised that any metadata scheme used or created is a highly political choice. Given that there are no universally accepted legally admissible metadata standards, efforts were made to develop a framework in consultation with a variety of international investigative bodies. Among these include consultations with members of the International Criminal Court, with members of the United Nations Office for High Commissioner of Human Rights, with members of the International, Impartial and Independent Mechanism on international crimes committed in Syria (IIIM), with archival institutes like the NIOD Institute for War, Holocaust and Genocide Studies, with international human rights organisations like Amnesty International, and with research institutes like the Human Rights Center at UC Berkeley School of Law.

Establishing a data ontology is necessary in order to assist users in identifying and understanding when, where, and what happened. A review of practices by other war archival institutes, such as those of NIOD, found that additional information is helpful for contextualising raw visual content (e.g. location of video recording; date of video recording and upload; and the origin of the video). Metadata collected by Syrian Archive includes description of the visual object as given (e.g. YouTube title); the source of the visual content; the original link where footage was first published; specific landmarks able to be identified; weather (which may be useful for geolocation or time identification); specific languages or regional dialects spoken; clothes or uniforms able to be identified; weapons or munitions used; device used to record the footage; and media content type. The metadata is populated automatically and manually depending on how it was collected from e.g open source or closed source. A detailed description and full list of metadata field types are provided on our website.

In categorising violations, Syrian Archive has decided to include the violations categories used by the Office of United Nations High Commissioner for Human Rights (OHCHR), however our data ontology was not limited by including only these categories. OHCHR categories were included because OHCHR is one of the groups in the unique position of being able to investigate incidents of human rights violations and war crimes. Categories identified by the UN OHCHR Inquiry on Syria and used by the Syrian Archive project include:

**Violations: treatment of civilians & hors de combat fighters**

* Massacres and other unlawful killing;  
* Arbitrary arrest and unlawful detention;  
* Hostage-taking;  
* Enforced disappearance;  
* Torture and ill-treatment of detainees;  
* Sexual and gender-based violence;  
* Violations of children's rights;

**Violations: conduct of hostilities**

* Unlawful attacks;  
* Violations against specifically protected persons and objects;  
* Use of illegal weapons;  
* Sieges and violations of economic, social and cultural rights;  
* Arbitrary and forcible displacement.  

Should potential investigations by international bodies not be pursued by the UN OHCHR and rather by another investigative body, it is anticipated that we will modify violations categories to meet the needs of those investigating.

## Preservation
The collection and secure preservation of the digital evidence workflow ensures that the original content is not lost due to removal on corporate platforms. This is done by collecting and securely storing digital content on external backend servers before it goes through basic verification. It is then backed up securely on servers throughout the world. Syrian Archive uses Sugarcube for this process, a free and open source software developed for use in human rights investigations using online-based user generated content research.


## Processing
Metadata from visual content collected from social media platforms is parsed and aggregated automatically using a predefined and standardised metadata scheme. Each unit of data in our database goes through our data pipeline.

In this pipeline we detect the language, standardise the data format (but keep the old format as well), as well as perform other transformations. We screenshot and download the web page we received the information from. Files that are in our database get both their `md5` and `sha256` hash. These get timestamped with [Enigio Time](https://www.enigio.com/) - a third party collaborator.

Processing also includes the parsing of video files into keyframes, as well as the use of machine learning software for object detection analysis through the use of [V-FRAME](vfame.io).

This prepares the visual content for initial verification. As much additional metadata and chain of custody information as possible is recorded. This is done to assist users in identifying and understanding when, where, and what happened in a specific incident.

## Verification
Verification is comprises of three components: 1) Verify the source of the video uploader; 2) Verify the location where the video was filmed; 3) Verify the dates in which the video was filmed and uploaded.

* Verify the source of the video uploader  

Establish that the source of the video on the Syrian Archive's verified list of credible sources. If the source is not an existing trusted source, determine the new source's credibility by going through the procedure highlighted above.

In some cases, near-duplicate content may be published. For example, if one video is 30 seconds and a second video is 10 minutes but includes all or portions of the first video, both videos would be published as long as it is possible to verify both videos. Similarly videos from news organisations or media houses featuring all or parts of content from other videos are also preserved, as long verification is possible. The Syrian Archive also preserves duplications if they are from different sources and the original uploader is unable to be determined (for example if two identical videos are uploaded simultaneously).

The video uploader source may not necessarily be the same as the source who originally filmed content. In most of the video footage verified by the Syrian Archive, only the video uploader and not the video filmer is known. Advanced verification in the analysis phase includes the source of filming, a process done in cases deemed priority.

* Verify the location where the video was filmed  

Each video goes through basic geolocation to verify that it has been captured in Syria. More in-depth geolocation is conducted for priority visual content in order to verify that it has been captured in a specific location. This has been done by comparing reference points (e.g. buildings, mountains ranges, trees, minarets) with Google Earth satellite imagery, Microsoft Bing, and Digital Globe, as well as OpenStreetMap imagery and geolocated photographs from Google Maps. In addition to this, the Syrian Archive has referenced the Arabic spoken in videos against known regional accents and dialects within Syria to further verify location of videos. When possible, the Syrian Archive has contacted the source directly in order to confirm the location, and cross-reference video content by consulting existing networks of journalists operating inside and outside Syria to confirm the locations of specific incidents.

* Verify the dates and times in which the video was filmed and uploaded  

The Syrian Archive verifies the date of capturing the video by cross referencing the publishing date of visual content collected from social media platforms (e.g YouTube, Twitter, Facebook and Telegram) with dates from reports concerning the same incident. Visual content collected directly from sources is also cross referenced with reports concerning the same incident featured in the video.

* News reports from international and local media outlets, including Reuters, Smart News Agency, Aleppo Media Center, Qasioun News Agency, LCC;  
* Human rights reports published by international and local organisations, including Human Rights Watch, Amnesty International, Syrian Human Rights Network, Violations Documentation Center in Syria, Syrian American Medical Society, and Physicians for Human Rights;  
* Reports shared by the Syrian Archive's network of citizen reporters on Twitter, Facebook and Telegram about the incidents.

Additional tools are used to check the date of the visual content such as Google reverse imagery and Sun Calc.

## Investigation and further analysis
In some cases, the Syrian Archive is able to conduct in-depth open source investigations. Time and capacity limitations means not all incidents are able to be analysed in-depth, however by developing a replicable workflow it is hoped that others can assist in these efforts of investigate other incidents using similar methods. A detailed overview of in-depth incident analysis is provided in the investigations page of our website.

### Witness Statements
For some incidents, Syrian Archive works with partner organisations to collect witness statements. In the past, this has included working with Syrians for Truth and Justice, Justice for Life, and others, who are tasked with collecting accounts of the survivors, such as those injured or their families, or accounts of eyewitnesses (e.g. medical staff, managers of hospitals)

### Flight Observation Data

To cross-reference with findings from visual content, flight observation data information is sometimes analysed by Syrian Archive to identify whether flights were observed in the vicinity of locations attacked for locations in which aerial bombings were alleged.

### Review
Once content have been processed, verified, and analysed, it is then reviewed for accuracy. In the event of a discrepancy, content is fed back into the digital evidence workflow for further verification. If content is deemed accurate it moves to the publishing stage of the digital evidence workflow. Regular reports on verified visual content ensure that the feedback loop between the Syrian Archive and sources who filmed the videos is closed. This allows Syrian Archive to add value to the visual content being preserved, verified and analysed immediately for advocacy purposes and later on for accountability and justice purposes.
