---
page: index
name: intro
---

Syrian Archive is a Syrian led project that aims to preserve, enhance and memorialise documentation of human rights violations and other crimes committed by all parties to conflict in Syria for use in advocacy, justice and accountability.
