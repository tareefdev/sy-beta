---
page: index
name: techAdvocacy
---

Hundreds of thousands of images, videos and text have been taken down by social media platform that are related to Syria's history and human rights situation. Archive your content independently with the Syrian Archive or here’s how to do it yourself.
