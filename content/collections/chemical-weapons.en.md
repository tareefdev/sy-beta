---
parent: collections
title: Chemical Weapons Database
date: 2018-04-24T00:05:18.055Z
desc: Summary of findings, about, and methods
image: /assets/chemical/chemicalheader.jpg
type: database
---
import {charts} from 'mnemonic-www';
import BlockViz from '../../src/components/charts/blockviz.js';
import RemovedVideos from '../../src/components/charts/removedvideos.js';

const {TimelineData} = charts;

## Chemical Weapons Database
Attacks using chemical weapons have been taking place in Syria since 2012. As of April 2018, official reports from the [French Foreign Ministry](https://www.diplomatie.gouv.fr/IMG/pdf/170425_-_national_evaluation_annex_-_anglais_cle81722e.pdf) and the [Commission of Inquiry on the Syrian Arab Republic](http://www.ohchr.org/SiteCollectionImages/Bodies/HRCouncil/IICISyria/COISyria_ChemicalWeapons.jpg) put the number of chemical attacks at 163.

The Syrian Archive has documented **212** chemical weapon attacks through documentation efforts of individuals and groups in Syria. This Chemical Weapons Database, released on 24 April 2018, is the first publicly available collection of open source content documenting chemical weapon attacks in Syria between 2012 - 2018.

The collection contains documentation from over **190 sources** that can be viewed, analysed and downloaded. This process not only identified an additional 50 attack sites in Syria but brought together **861 verified videos** to corroborate these attacks.

The Syrian Archive is a civil society group that has been documenting the Syrian conflict since 2014. To date over 1.4 million videos have been located and preserved. Like many monitoring organisations, the Syrian Archive is unable to go into Syria to investigate these attacks. Relying on a network of journalists and video makers it is essential to monitor, document and report on the crimes in Syria and preserve these pieces of evidence for accountability and justice initiatives.

## These videos include
- Casualties as a result of the chemical attacks
- People being treated in medical facilities and other locations including children and women as a result of the chemical attacks
- Remnants of munitions used to deliver chemical weapons
- Dead animals and plants as a result of chemical attacks
- Chemical weapon impact sites, including civilian homes
- Gas cylinders reported to contain chlorine being thrown from helicopters;
- Syrian army preparing munitions for launch
- Witness testimonies of chemical attacks from victims, medical workers, and civilians through video interviews of the chemical attack
- Information about locations of chemical attacks
- Details on names of people killed or affected as a result of the chemical attacks
- Rescue missions by humanitarian groups to help victims of the chemical attacks
- Yellow smoke as a result of gas cylinder attacks
- Destruction of buildings as a result of chemical attacks
- Symptoms of victims being affected by toxic chemicals
- Casualties as a result of chemical attacks being buried
- Collecting samples from the impact sites to be analysed for traces of chemical substances
- Engineering experts removing the remnants of unexploded munitions

###### Number of Videos
<BlockViz 
	width={700}
	numbers={[862]}
	colors={["teal"]}
/>

###### Sources of data
<BlockViz 
	width={700}
	numbers={[193]}
	colors={["red"]}
/>

###### Chemical weapon attacks
<BlockViz 
	width={700}
	numbers={[162,50]}
	colors={["SkyBlue","orange"]}
/>

## The use of chemical weapons in Syria
International Humanitarian Law [prohibits the use of chemical munitions](https://ihl-databases.icrc.org/customary-ihl/eng/docs/v1_rul_rule74) in both international and non-international conflicts.

In 2013 the Syrian Arab Republic [ratified the Chemical Weapons Convention](http://www.securitycouncilreport.org/atf/cf/%7B65BFCF9B-6D27-4E9C-8CD3-CF6E4FF96FF9%7D/s_res_2118.pdf), following findings by the [Organisation for the Prohibition of Chemical Weapons](https://www.opcw.org/) that sarin had been used [on 21 August 2013](http://www.securitycouncilreport.org/atf/cf/%7B65BFCF9B-6D27-4E9C-8CD3-CF6E4FF96FF9%7D/s_2013_553.pdf). In 2014, after an in-country mission, [OPCW reported they destroyed](https://www.opcw.org/news/article/opcw-all-category-1-chemicals-declared-by-syria-now-destroyed/) 100% of Category 1 chemicals by the Syrian Arab Republic and 70% of Syria’s Category 2 chemicals. They also reported destroying 94% of it’s declared chemical stockpile.

That chemical weapons were used in attacks attributed to the Syrian government after OPCW [destroyed chemical weapons stockpiles](https://www.opcw.org/fileadmin/OPCW/CSP/C-20/en/c2004_e_.pdf) indicates that either not all stockpiles were destroyed or that the Syrian government and non-governmental armed groups involved in the Syrian conflict (e.g. Daesh) acquired or manufactured chemical weapons after August 2013.

Syria had previously acceded to the Protocol for the Prohibition of the Use in War of Asphyxiating, Poisonous or Other Gases and of [Bacteriological Methods of Warfare on 22 November 1968](http://www.securitycouncilreport.org/atf/cf/%7B65BFCF9B-6D27-4E9C-8CD3-CF6E4FF96FF9%7D/s_res_2118.pdf).

By examining the 212 incidents in which chemical weapons were used in the Syrian conflict since 2012 it becomes clear that their use is not infrequent or random but rather a repeated, deliberate and illegal [strategy of war](https://www.hrw.org/news/2017/02/13/syria-coordinated-chemical-attacks-aleppo).

The Syrian Archive has identified patterns in the use of chemical weapons its verified dataset. These include patterns include the use of chemical attacks by the Syrian army when trying to advance to control specific areas. Between 2013-2018 Jobar frontlines saw 12 chemical attacks.

Chemical attacks were also used by the Syrian army when forces not able to control specific areas after trying to advance, such as Kafr Zita 2014 and Aleppo 2016. Areas out of the control of the Syrian government also experienced chemical attacks perpetrated by the Syrian government, such as in Khan Shaykhun on 4 April 2017.

Analysis of the 861 verified videos indicates the types of chemicals used in some of the attacks. In 62 videos documentation of chlorine was observed. The effects of sarin can be observed in 372 videos. Containers and remnants of mustard gas were observed in four videos.

Through comparing a temporal analysis of chemical weapons attacks with territorial maps published by conflict monitoring groups (e.g. [liveuamap](https://syria.liveuamap.com/)), as well as information gathered through Syrian Archive’s networks, it becomes apparent that the majority of attacks occured in opposition controlled areas between 2012 and 2018.

----------------------

Syrian Archive has identified 85 unique locations which were attacked using chemical weapons. The Damascus countryside experienced the majority of chemical weapons attacks with 61 attacks documented. Idlib experienced 49 chemical weapons attacks. Aleppo and Hama experienced 33 and 29 chemical weapons attacks respectively. Homs experienced six chemical weapons attacks. Daraa experienced three chemical weapons attacks, and Deir Ezzor experienced one.

In their research of 130 chemical attacks, the [French Foreign Ministry](https://www.diplomatie.gouv.fr/IMG/pdf/170425_-_national_evaluation_annex_-_anglais_cle81722e.pdf) attributed five chemical weapon attacks in which sarin was used to the Syrian government (five proven, two with strong presumption). The Foreign Ministry additionally claimed there was a strong presumption of chlorine used by the Syrian government in 22 incidents. Three chemical weapon attacks using mustard gas were also attributed by the Foreign Ministry to Daesh. The Foreign Ministry did not attribute 98 chemical attacks.

----------------------

Syrian Archive provides corroborating open source content to findings published by the Organisation for the Prohibition of the Chemical Weapons, the UN Commission of Inquiry on the Syrian Arab Republic, the French Foreign Ministry, and many Syrian human rights documentation groups with visual documentation [verified](https://stage.syrianarchive.org/en/tools_methods) by the Syrian Archive team, and does so in an publicly accessible format.

Visual documentation included in the Syrian Archive Chemical Weapons Database finds corroborating open source documentation for 103 of the 130 incidents published by the French Foreign Ministry. For 24 incidents included in French Foreign Ministry findings, the Syrian Archive was either unable to locate visual documentation or concluded that conventional weapons were at play, as of the date of publication. To give one example, the French Foreign Ministry reported a chemical attack on 6 September 2016 in Ein Terma and/or Jobar. After collecting and reviewing video documentation including testimony by medical workers treating those affected, the Syrian Archive concluded that suffocation was the result of conventional weapons rather than chemical weapons.

For ten incidents published by the French Foreign Ministry, the Syrian Archive provided specific locations where none were indicated. For example, Aleppo is noted as the location in 19 incidents, but the specific town and location were not provided.

The French Foreign Ministry additionally identified three reported chemical weapons attacks, claiming they took place on 25 April 2015 and 26 April 2015 (1- Sahl Al Ghab, 2- Hama, 3- Al Hawash). Through open source analysis, the Syrian Archive has concluded that these three incidents were in fact one incident which occurred on 26 April 2015 in Hawash, a village in Hama province. The Syrian Archive has identified two YouTube videos and eight Facebook posts, published from the location of the chemical attack, confirming these three incidents are in fact one.

<TimelineData
locale={"en"}
width={700}
height={200}
api={"https://api.betasyrianarchive.tk"}
/>

## The Data
This Chemical Weapons Database contains **28 GB** of documentation of **212** chemical weapon attacks in Syria during the 2012-2018 period. This data comes from **193 sources** made up of individual citizen journalists, local and international media groups, as well as NGOs and civil society organisations. It is important to note that many if not all of these sources are partisan, and thus require caution with regards to their claims.

A total of 861 relevant videos documenting chemical weapon attacks were identified, the majority of which were published on YouTube. Additional documentation from Facebook, Twitter, and other social media platforms will be incorporated into the database in the coming months.

###### Videos Removed From Youtube
<small> as of 20.4.2018 </small>
<RemovedVideos 
	width={800}
/>

The sheer amount of content being created, and the near constant removals of materials from public channels means that the Syrian Archive is in a race against time to preserve important documentation of crimes committed. Content preserved and verified by the Syrian Archive might offer the only evidence to corroborate witness testimonies of chemical weapons attacks, and to implicate potential perpetrators.

Of the 861 videos included in this dataset, 9% had been removed on YouTube. Due to Syrian Archive’s technical infrastructure and the constant monitoring of the status of videos and channels, the termination of affected content was brought to YouTube’s attention, resulting in 13 channels of media groups documenting chemical weapons attacks being reinstated in cases where users did not remove content willingly. One channel, “شبكة أحرار دوما - ريف دمشق,” by media reporters that had been documenting violations committed in Douma since 2012 has over 1.600 videos and one million views.

## Using the Database
The Syrian Archive created the Chemical Weapons Database for a variety of audiences working on human rights monitoring in Syria. This includes journalists, who are be able to quickly find verified materials relating to hundreds of incidents, non-governmental organisations who use this material in their advocacy work, and lawyers, who use materials to seek accountability of perpetrators of these attacks.

**Of importance to note is that much of the content contained in the database is highly graphic. It is recommended users take precautions in viewing material. **

## Methodology
The Syrian Archive created a list of incidents based off publicly available research by the French Foreign Ministry, the United Nations Independent International Commission of Inquiry on the Syrian Arab Republic, findings from various OPCW fact-finding missions (including those by the OPCW-UN Joint Initiative Mechanism), and reports from civil society organisations (e.g. Human Rights Watch).

Syrian Archive staff sifted through its existing database of over 1.4 million videos to identify documentation of chemical attacks. Relevant videos were located and verified using the [Syrian Archive Digital Evidence Workflow](https://stage.syrianarchive.org/en/tools_methods) and metadata was extracted and standardised using an automated process and metadata scheme. Through undertaking this process, incidents not included on any of the reports mentioned above were uncovered.

For each of the 212 incidents identified, a detailed process of discovery was initiated. This process necessitated:
Extracting units from the Syrian Archive database about specific incidents using dates and locations;
Extracted units through specific geo-based areas;
Using content discovery tools for social media content such as [whopostedwhat.com](https://whopostedwhat.com).

Through adding context to data, the Syrian Archive transformed digital content to useful and searchable information with querying capabilities.

All content used in the building of this collection is open source and publicly derived, processed and published. The database places incidents on a map and allows data to be searched by incidents, keyword, and related video content.

While comprehensive in scope, there exist gaps in the collection. Some incidents are heavily documented while others are not. Continual updates of observations and incidents are planned, as well as inclusion of data from additional sources and publishing platforms (e.g. Twitter and Facebook).

## About Syrian Archive
Syrian Archive is a Syrian-led and initiated collective of human rights activists dedicated to preserving, verifying and investigating open-source documentation relating to human rights violations committed by all sides during the Syrian conflict and to developing innovative open-source tools and methodologies to assist in these efforts.

The Syrian Archive team is uniquely comprised of researchers, journalists, technologists and digital security experts who have been working in the field of human rights, verification, open source technologies and investigation methodologies for the past decade. They are motivated in their mission to enable various stakeholders such as lawyers, journalists and activists, to be able to find, store and use publicly available content to not only document human rights violations but to prosecute those who have perpetrated and enabled these crimes.

## Video submission
The Syrian Archive seeks to provide a comprehensive and accurate open source database on chemical weapons attacks, and is always seeking additional documentation to add to its collection. If you have a video or information which is not in the dataset and would like to send this to the Syrian Archive please email  info@syrianarchive.org [(PGP key)](http://pgp.mit.edu/pks/lookup?op=vindex&search=0x0AF7BA93CD64500E) to inquire as to how do do this safely.

## Errors, corrections and feedback
The Syrian Archive strives for accuracy and transparency of process in our reporting and presentation. That said, it is recognised that the information publicly available for particular events can, at times, be limited. Our video datasets are therefore organically maintained, and represent our best present understanding of alleged incidents.
If you have new information about a particular event; if you find an error in our work - or if you have concerns about the way we are reporting our data - please do engage with us. You can reach us at info@syrianarchive.org.

## Acknowledgements
The videos in this database have been collected by groups and individuals living or working in Syria. This collection would not be possible without those documenting and reporting on war crimes and crimes against humanity. Currently in this database there are 240 different sources, our thanks to each of them.  

The Syrian Archive would like to thank the student groups, in particular the Human Rights Center Investigations Lab, UC Berkeley, for their work verifying, annotating, and analysing videos. In particular we would like to thank: Youstina Youssef, Amy Choi, Anna Banchik, Annabell Brockhues, Ariela Levy, Carlos Sanchez, Caron Creighton, Charlotte Godart,  Elaria Youssef, Elise Baker, Félim McMahon, Greg Waters, Hannah Ellis, Manu Chaturvedi, Mariya Katsman, Michael Elsanadi, Monica Namo, Natalia Krapiva, Natalie Fowler, Rachel Thampapillai and Sarah Solieman.

This project was partially made possible by The Prototype Fund project of the Open Knowledge Foundation Germany, funded by the Federal Ministry of Education and Research (BMBF) in 2016, volunteers and individual donations. To donate to the project, visit our Patreon page.

