---
parent: collections
title: Russian Airstrikes Database
date: 2018-10-31T00:05:18.055Z
desc: Documentation of alleged Russian airstrikes targeting civilians and civilian infrastructure
image: /assets/collections/russian-airstrikes-database.png
type: database
---

## Russian Airstrikes Database

Several years of monitoring alleged Russian airstrikes in Syria reveals a pattern of indiscriminate targeting of civilians and civilian infrastructure. In an analysis of **3303 videos** documenting alleged Russian airstrikes from **116 sources** between **30 September 2015 and 9 September 2018**, Syrian Archive has identified **1418 incidents** in which Russian forces allegedly targeted civilians or civilian infrastructure of little to no military value. Content included in this database can be viewed, analysed and downloaded.

While data presented in this collection does not include all incidents of alleged Russian airstrikes on civilians between 2015 and 2018, it presents all incidents for which visual content was available and verifiable as of the date of publication. Syrian Archive hopes this will support reporting, advocacy, research, and accountability efforts.

### These databases includes verified videos that show:

- Airstrikes published by the Russian Ministry of Defence (MoD)
- Casualties as a result of alleged Russian attacks including children and women
- People being treated in medical facilities and other locations including children and women as a result of the alleged Russian attacks
- Remnants of munitions used in alleged Russian attacks
- Alleged Russian attacks impact sites, including civilian homes, mosques, schools, bakeries, hospitals, markets, and cultural properties
- Alleged Russian attacks against humanitarian relief personnel and objects
- Alleged Russian attacks on water sources
- Witness testimonies of alleged Russian attacks from victims, medical workers, and civilians through video interviews
- Information about locations of alleged Russian attacks
- Details on names of people killed or affected as a result of alleged Russian attacks
- Rescue missions by humanitarian groups to help victims of the alleged Russian attacks
- Engineering experts removing the remnants of unexploded munitions
- Russian aircrafts launching airstrikes on civilian locations

---

This open source database is fully searchable and queryable by date, location, keyword, relevance, and confidence score. It consists of two collections: 1) various documentation efforts of citizen journalists, media groups, humanitarian groups, human rights groups and others in Syria, and 2) archived and geolocated videos published by the Russian Ministry of Defence. Both collections have been fully verified by Syrian Archive staff.

---


### Added value
There have been several efforts by various parties to document atrocities and human rights violations committed by Russian forces in Syria. These include efforts by the Commission of Inquiry on the Syrian Arab Republic and by many human rights monitoring organisations (e.g., Syrian Network for Human Rights, Violations Documentation Center, Syrian Observatory for Human Rights, Syrian American Medical Society, Syrians for Truth and Justice, and many others).

The added value of the publication of this collection is that every piece of data is preserved, verified, standardised, and clustered into incidents.

Additional metadata has been added to the database, making it to be searchable. This is useful for journalists, lawyers and human rights monitors, and investigators for reporting, advocacy and accountability work.

It is possible to search the database using keywords. This means a user can search for attacks, filtered by the use of particular munitions, a particular location, or a particular type of target (e.g., medical facilities). Results can be sorted by relevance, date or the number of videos and reports provided for each incident. The map and timeline can be used to see the scale and breadth of attacks.

---

Syrian Archive is a civil society group that has been documenting the Syrian conflict since 2014. To date over 3.3 million units of digital content have been located and preserved. Like many monitoring organisations, Syrian Archive is unable to go into Syria to investigate these attacks. Relying on a network of journalists and video makers it is essential to monitor, document and report on the crimes in Syria and preserve these pieces of evidence for accountability and justice initiatives.

## Russian involvement in Syria

While Russia has been supporting the Syrian government since 2011, including the use of its veto power at the security council several times until 2018, their official involvement in military activities began in 2015. On 30 September 2015, the Russian Federal Council authorised President Vladimir Putin to use force in Syria announcing the [beginning of Russian military operations in Syrian territories](https://www.youtube.com/watch?v=qblmOvkV9KM).

That same day, spokesman of the Russian Ministry of Defence, Major General "Igor Konashenkov" stated that [Russian forces launched roughly twenty airstrikes](https://www.youtube.com/watch?v=qblmOvkV9KM), of which, precision strikes targeted eight ISIS sites in Syria. He stated: “These strikes resulted in the destruction of stores of ammunition, weapons, fuel and weapons installations. Command and control centres belonging to the armed group of the Islamic State were also completely destroyed. However the Russian air force has not targeted any civilian infrastructure.”

The official website of the Russian Ministry of Defence [published a video](http://eng.mil.ru/en/news_page/country/more.htm?id=12059172@egNews) showing several sites in Syria being targeted on 30 September 2015. The MoD stated that the aircraft that carried out the eight high-precision strikes against “ISIS” took off From Bassel al-Assad Airport (Hmeimim military base), located in the countryside of Latakia province. The Russian MoD website [also announced that](https://structure.mil.ru/mission/fight_against_terrorism.htm): “From September 30, 2015, [the Ministry of Defence of the Russian Federation] conducted an air operation to combat international terrorist groups in the Syrian Arab Republic.”

![Russian Ministry of Defence announcing air operation in Syria](/assets/russian/ru-mod-website.png)

That same day between 10:30 - 11:30 AM, several [news networks](https://www.youtube.com/watch?v=KqEtOd5Mypg) and [organisations](http://www.vdc-sy.info/pdf/reports/1444252328-Arabic.pdf) reported Russian airstrikes in the town of Talbisa in Homs’ countryside, an attack which used 5 missiles. Five other airstrikes occurred in other towns and cities in Homs’ countryside that same day, including in [Al Zafarana, Al Makremiya](https://vdc-sy.net/report-on-russian-attacks-ar/), and [Al Rastan](https://www.youtube.com/watch?v=lIX_NE2R5ms). The locations of those initial attacks were not in ISIS controlled areas based on a [Russian Ministry of Defence map](https://vk.com/wall-3457550_23655) published on the official VK account of the Russian Federation Armed Forces.


### De-Escalation Zones:

Russia, along with Iran and Turkey, [signed the De-Escalation Zone agreement](https://www.youtube.com/watch?v=5cF-gIL8yzk) in May 2017. This agreement was originally intended to establish a ceasefire in Idlib province, parts of Latakia, Hama, Aleppo, Homs, Damascus’ countryside, Dara’a and Quneitra provinces. However its true motive has been heavily criticised as, since the signing of the agreement, Russia conducted hundreds of airstrikes against areas within the De-Escalation zone. Syrian Archive [published investigations](https://syrianarchive.org/en/investigations) about attacking public markets and medical facilities that is located in the de-escalation zone. Below is a [map published](https://function.mil.ru/news_page/intrel/more.htm?id=12121964%40egNews) by the Russian Ministry of Defence that shows the de-escalation zone. (de-escalation zones in blue, ISIS in grey, Syrian army in orange).

![De-escalation Zone Map](/assets/russian/ru_de-escalation_zone.png)

The Russian Ministry of Defence published a [video](https://www.youtube.com/watch?v=viCiwbJG5Pk) on their official YouTube channel on August 22nd 2018 titled “Military operation of the armed forces on the Russian Federation in the Syrian Arab Republic”. The video summarizes the operation that the Russian military carried in Syria since September 2015. The video included the following: “During the active phase of operation, the intensity of use of the aviation reached more than 100 plane flights per day. Each plane was operating 3-4 flights daily. The maximum amount was 139 flights per day, on 20 November 2015.”

The video also mentioned Russian aircraft used in carrying out its military activities. This includes a total of 34 planes (12 Su-24, 10 Su 25CM, 4 Su 30CM, 4 Su-34, 2 Su-25UB, 1 An-30,1 Il-20), 16 helicopters (12 Mi 24p, 4 Mi-8MTSh) used, as well as other types of weapons and vehicles. It also mentioned 1902 humanitarian convoys were supported providing food and medication by the Russian Ministry of Defence.

The video did not mention anything about civilian harm as a result of attacks carried out by the Russian air force in Syria. As such, Syrian Archive is highlighting civilian harm as a result of alleged Russian airstrikes in this database and report.

## Pattern of attacks

Syrian Archive was able to identify a total of 205 unique incidents (433 videos) in which violations against specifically protected persons and objects resulted from alleged Russian airstrikes. The Syrian Archive discovered, preserved, and verified videos about those incidents.

These are not all incidents against specifically protected persons and objects occurred between 2015 and 2018 and include only incidents for which Syrian Archive has verified visual documentation.

A timeline of weekly alleged Russian airstrikes on Syrian civilians and civilian infrastructure is provided below:

---

<iframe src="https://public.tableau.com/views/AllegedRussianairstrikes2015-2018/Sheet1?publish=yes:showVizHome=no&:embed=true" width="750" height="600"></iframe>

---

This timeline shows since Russian involvement, alleged airstrikes have occurred consistently. During the September 2015 to February 2016 period, Russian airstrikes on civilian infrastructure were quite frequent. This was followed by a period between March 2016 and March 2017 in which attacks occurred on a less regular basis. In 2017, alleged Russian airstrikes increased and peaked during the April and September periods. During 2018, a alleged Russian airstrikes peaked during the first week of January and February.

Below is a table that summarises the number of incidents and videos per category between September 30th 2015 and September 09th 2018.

---

| *Category*                                                   | *Number of incidents* | *Number of videos* |
|------------------------------------------------------------|---------------------|------------------|
| Russian airstrikes in Syria                                | 1418                | 3303             |
| Civilian casualties as a result of alleged Russian attacks | 704                 | 2184             |
| Attacks claimed by Russian Ministry of Defense             | 242                 | 172              |
| Attacks on other civilian infrastructure                   | 52                  | 63               |
| Attacks against hospitals                                  | 35                  | 164              |
| Attacks against markets                                    | 27                  | 64               |
| Attacks against mosques                                    | 27                  | 45               |
| Attacks against schools                                    | 23                  | 41               |
| Attacks against humanitarian relief personnel and objects  | 18                  | 28               |
| Attacks against bakeries                                   | 12                  | 17               |
| Attacks against water sources                              | 6                   | 5                |
| Attacks against cultural property                          | 3                   | 4                |



---

### Highlighted incidents

#### Airstrikes against medical facilities

Much has been written about attacks targeting hospitals in Syria (e.g., UN OHCHR, SAMS, Physicians for Human Rights, Bellingcat, and many other organisations). For this reason, and in order to not duplicate findings, deeper analysis will not be provided in this report. Attacks against hospitals are illegal under International Humanitarian Law as they fall under the category of specially protected persons and objects. An analysis of 164 collected and verified videos by the Syrian Archive found a total number of 35 incidents in which hospitals were attacked.

##### Kafranbel Surgical Hospital

On 5 February 2018, local sources including Muaz Al Shami reported that Kafranbel Surgical Hospital (also known as Hand in Hand Hospital and Orient Hospital), was struck by multiple airstrikes resulting in significant material damage which caused the entire hospital to become completely out of service. Open source materials and flight observation data indicates this attack was conducted by the Russian air force.

Footage filmed during and after the 5 February 2018 attack clearly shows the hospital was attacked directly, with [one geolocated video capturing a bomb](https://www.youtube.com/watch?v=WzFAjvqC9uw&list=PLPC0Udeof3T4QmV4f9tEhDmQzr2Z_vTKZ&index=1) as it fell through the air and struck the east side of the hospital building. See below:

![Kafranbel Surgical Hospital](/assets/russian/Kafranbel-Surgical-Hospital.jpg)


DigitalGlobe satellite imagery from documenting before and after the attack  can be seen below:

![Before and after Satellite imagery of Kafarnbel](/assets/russian/Kafranbel-Surgical-Hospital-satellite.png)

Syrian Archive has written a more extensive report on this attack, available within the [Medical Facilities Under Fire report](https://syrianarchive.org/en/investigations/second-medical-facilities-under-fire/orient.html )

---

### Airstrikes against public markets

The Syrian Archive was able to identify 27 unique incidents in which markets were attacked based on an analysis of 64  videos. All videos are available on the Syrian Archive website and a summary of relevant information (including metadata), is included in every incident.

#### Atarib market

On 13 November 2017 between 14:07 and 14:11 Damascus time, the Atarib market and police station were targeted by three airstrikes resulting in the death of 69 civilians, significant damage to several buildings on the Atarib market street, as well as damage  to the Free Syrian police building.

![Atarib market](/assets/russian/Atarib-market.png)

Open source materials indicate this attack was conducted by either the Russian or Syrian air force. 69 people were killed and 100 people were injured as a result of this attack.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/0X128QHTd3k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

DigitalGlobe before and after satellite imagery of the market can be seen below.

![Before and after satellite imagery](/assets/russian/Atarib-market-satellite.png)

Syrian Archive has written a more extensive report on this attack, available within the [investigations section of its website](https://syrianarchive.org/en/investigations/Atarib-Market-Bombing.html)

---

### Airstrikes against water sources

The Syrian Archive was able to identify 6 unique incidents in which water were attacked based on an analysis of 5 videos. All videos are available on the Syrian Archive website and a summary of relevant information (including metadata), is included in every incident.

The Russian Ministry of Defence released a [video](https://www.youtube.com/watch?v=vKbA_g-4zEE) on their official YouTube channel with a title “Airstrike against oil refinery near Khafsa Kabir”.

![Russian MoD video](/assets/russian/Khafsa-Kabir-ru-youtube.png)

However, this “oil refinery” was actually a water treatment facility, which, according to the United Nations Children’s Fund (UNICEF) representative in Syria, Hanaa Singer,  “produced an average of 18 million liters of water daily.” The structure shown in the airstrike video appears identical to Google Earth satellite imagery as shown below:

![Khafsa satellite imagery](/assets/russian/Khafsa-satellite.png)

Russia Today published a video on 15 July 2017 in Arabic about the renovation operations of Khafsa water treatment facility.

Full investigation [can be seen here](https://www.bellingcat.com/news/mena/2015/12/11/syrias-bombed-water-infrastructure/)

---

### Airstrikes against mosques

The Syrian Archive was able to identify 27 unique incidents in which mosques were attacked based on an analysis of 45 videos. All videos are available on the Syrian Archive website and a summary of relevant information (including metadata), is included in every incident.

#### Omar bin Al Khattab mosque

On October 1st 2015 at 12:00 pm Damascus time, Omar bin Al Khattab mosque was targeted with an alleged Russian airstrike as claimed by media and humanitarian groups reports began appearing online. The Russian Ministry of Defence [published a satellite image](https://www.facebook.com/permalink.php?story_fbid=1674425756133507&id=1492252324350852) on their official Facebook page saying that the claims were another “hoax”.  

![Russian MoD satellite imagery](/assets/russian/al-farooq-omar-bin-al-khattab-mosque.png)

The airstrike resulted of structural damage to the mosque as well as 1 killed and 8 injured people.

Google earth satellite imagery documenting the mosque before and after the attack can be seen below.

![DigitalGlobe before and after satellite imagery](/assets/russian/al-farooq-omar-bin-al-khattab-mosque-satellite.png)

Full investigation [can be seen here](https://syrianarchive.org/en/investigations/Fact-Checking-Russias-Claim-That-It-Didnt-Bomb-A-Mosque.html)  

## Munitions Identified

Through its analysis of video documentation of alleged Russian airstrikes on civilians and civilian infrastructure, Syrian Archive identified **63 incidents** in which cluster munitions were used, **39 incidents** in which incendiary weapons were used, and **130 incidents** in which thermobaric weapons were used. For all incidents identified, munitions were used unlawfully against civilians in Syria.

**These figures are not all incidents in which alleged Russian airstrikes targeted specifically protected persons and objects. Rather, they refer only to incidents for which Syrian Archive has preserved and verified video documentation. The Syrian Archive will be using different types of tools including computer vision tools to discover more content about the use of illegal munitions.**

Although Russia has, through official spokespersons, denied using cluster munitions in Syria, Russia Today was caught editing 20 June 2016 footage of Russian jets at Hmeymim airbase in Syria which inadvertently had showed RBK-500 ZAB 2.5SM incendiary munitions. Below is a video published by the [Conflict Intelligence Team](https://citeam.org) concerning this:

<iframe width="100%" height="315" src="https://www.youtube.com/embed/OIbLqaOfnGg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Unlike the Syrian government, Russia has signed up to the Convention on Conventional Weapons (CCW) protocol prohibiting the use of air-dropped incendiary weapons in areas containing concentrations of civilians. This makes their use by Russian forces in Syria even more troubling.

The Syrian Archive will soon be releasing a verified video database concerning the use of illegal munitions by Syrian and Russian forces. Those videos were discovered manually by our researchers as well as automatically using [VFRAME](https://vframe.io/) as shown below:

<iframe src="https://player.vimeo.com/video/297847125" width="100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

## The Data
This database contains **40.7 GB** of documentation of **1418 attacks in Syria** allegedly as a result of Russian airstrikes during the 2015-2018 period. This data comes from 116 sources made up of individual citizen journalists, local and international media groups, as well as NGOs and civil society organisations. It is important to note that many if not all of these sources are partisan, and thus require caution with regards to their claims.

A total of **3303 relevant videos** documenting alleged Russian airstrikes attacks were identified, the majority of which were published on YouTube. Additional documentation from Facebook, Twitter, and other social media platforms will be incorporated into the database in the coming months.

The sheer amount of content being created, and the near constant removals of materials from public channels means that the Syrian Archive is in a race against time to preserve important documentation of crimes committed. Content preserved and verified by the Syrian Archive might offer the only evidence to corroborate witness testimonies of Russian attacks, and to implicate potential perpetrators.

### Using the Database
Syrian Archive created the Database for a variety of audiences working on human rights monitoring in Syria. This includes journalists, who are be able to quickly find verified materials relating to hundreds of incidents, non-governmental organisations who use this material in their advocacy work, and lawyers, who use materials to seek accountability of perpetrators of these attacks.

*Of importance to note is that much of the content contained in the database is highly graphic.*

It is recommended users take precautions in viewing material.

## Methodology

Syrian Archive created a list of incidents based off publicly available research by the United Nations Independent International Commission of Inquiry on the Syrian Arab Republic, reports from many Syrian and International civil society organisations as well as journalists, activists, lawyers and individuals claiming that the attacks were conducted by the Russian air force. We have used flight observation data of aircrafts flying from Hmeymim airbase into the impact sites to corroborate with the mentioned claims. We were not able to do this corroboration for all incidents in this database. We are releasing this database online to allow for collaborative verification and corroboration for each incident that hasn’t been fully investigated.

Syrian Archive staff sifted through its existing database of over **2.5 million videos** to identify documentation of targeting civilians and civilian infrastructure as a result of alleged Russian airstrikes. Relevant videos were located and verified using the [Syrian Archive Digital Evidence Workflow](https://syrianarchive.org/en/tools_methods). Metadata was extracted and standardised using an automated process and metadata scheme. Through undertaking this process, incidents not included on any of the human rights reports that were published by different organisations were uncovered.

For each of the **1418 incidents** identified, a detailed process of discovery was initiated. This process necessitated:

Extracting units from the Syrian Archive database about specific incidents using keywords, dates and locations;
Extracted units through specific geo-based areas;
Using content discovery tools for social media content such as whopostedwhat.com;
Using computer vision tools to extract videos showing the use of cluster munitions.

Through adding context to data, Syrian Archive transformed digital content units into useful and searchable information with querying capabilities.

All content used in the building of this collection is open source and publicly derived, processed and published. The database places incidents on a map and allows data to be searched by incidents, keyword, and related video content.

While comprehensive in scope, there exist gaps in the collection. Some incidents are heavily documented while others are not. Continual updates of observations and incidents are planned, as well as inclusion of data from additional sources and publishing platforms.

## Errors and Corrections
### Video submission
The Syrian Archive seeks to provide a comprehensive and accurate open source database on chemical weapons attacks, and is always seeking additional documentation to add to its collection. If you have a video or information which is not in the dataset and would like to send this to the Syrian Archive please email  info@syrianarchive.org (PGP key - link) to inquire as to how do do this safely.

## Errors, corrections and feedback
Syrian Archive strives for accuracy and transparency of process in our reporting and presentation. That said, it is recognised that the information publicly available for particular events can, at times, be limited. Our video datasets are therefore organically maintained, and represent our best present understanding of alleged incidents.

## Acknowledgements
The videos in this database have been collected by groups and individuals in Syria.

This collection would not be possible without those documenting and reporting on war crimes and crimes against humanity. Currently in this database there are 116 different sources, our thanks to each of them.

Special thanks to everyone who helped in verifying, annotating and analysing videos.

In particular we would like to thank:
Mohammad Mhanna, Lilas Al Boni, Guevara Nabi, Ruba Akil, Nawar Mohra, Gretel and Gabi.

## About Syrian Archive

Syrian Archive is a Syrian-led and initiated collective of human rights activists dedicated to preserving, verifying and investigating open-source documentation relating to human rights violations committed by all sides during the Syrian conflict and to developing innovative open-source tools and methodologies to assist in these efforts.

The Syrian Archive team is uniquely comprised of researchers, journalists, technologists and digital security experts who have been working in the field of human rights, verification, open source technologies and investigation methodologies for the past decade. They are motivated in their mission to enable various stakeholders such as lawyers, journalists and activists, to be able to find, store and use publicly available content to not only document human rights violations but to prosecute those who have perpetrated and enabled these crimes.
