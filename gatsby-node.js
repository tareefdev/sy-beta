const path = require(`path`);
const locales = require(`./config/i18n`);
const { createFilePath } = require(`gatsby-source-filesystem`);
const { fmImagesToRelative } = require("gatsby-remark-relative-images");

const {
  findKey,
  removeTrailingSlash
} = require(`./src/utils/gatsby-node-helpers`);

exports.onCreatePage = ({ page, actions }) => {
  const { createPage, deletePage } = actions;
  deletePage(page);

  Object.keys(locales).map(lang => {
    const localizedPath = (function() {
      if (locales[lang].default && page.path === "/") {
        return "/";
      } else {
        return `${locales[lang].path}${page.path}`;
      }
    })();
    return createPage({
      ...page,
      path: removeTrailingSlash(localizedPath),
      context: {
        ...page.context,
        locale: lang,
        dateFormat: locales[lang].dateFormat
      }
    });
  });
};

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions;
  fmImagesToRelative(node); // convert image paths for gatsby images
  if (node.internal.type === "Mdx") {
    const filePath = node.fileAbsolutePath;
    const pathAsArray = filePath.split("/");
    const relativePath = pathAsArray.splice(pathAsArray.indexOf("content"));
    const parentDir = relativePath[relativePath.length - 2];
    const postType = relativePath[1];
    createNodeField({ node, name: `parentDir`, value: parentDir });
    createNodeField({ node, name: `postType`, value: postType });

    const name = path.basename(node.fileAbsolutePath, `.md`);
    const defaultKey = findKey(locales, o => o.default === true);
    const isDefault = !(name.slice(-3) === ".ar");
    const lang = isDefault ? defaultKey : name.split(`.`)[1];
    createNodeField({ node, name: `locale`, value: lang });
    createNodeField({ node, name: `isDefault`, value: isDefault });

    let value = createFilePath({ node, getNode }).slice(0, -4);
    if (value.includes("index") && !value.includes("html")) {
      value = `${value}.html`;
    }
    createNodeField({
      name: `slug`,
      node,
      value
    });
  }
};

exports.createPages = async ({ graphql, actions }) => {
  const { createPage, createRedirect } = actions;
  createRedirect({ fromPath: "/en", toPath: "/", isPermanent: true });
  const blogTemplate = path.resolve(`./src/templates/blog-post.js`);
  const unitTemplate = path.resolve(`src/templates/unit-page.js`);
  return graphql(
    `
      {
        allMdx(
          sort: { fields: [frontmatter___date], order: DESC }
          filter: { frontmatter: { title: { ne: "" } } }
        ) {
          edges {
            node {
              fields {
                slug
                locale
                isDefault
                parentDir
              }
              frontmatter {
                title
              }
            }
          }
        }

        allUnitsJson {
          edges {
            node {
              id
              lang
            }
          }
        }
      }
    `
  ).then(result => {
    if (result.errors) {
      throw result.errors;
    }

    const posts = result.data.allMdx.edges;
    const units = result.data.allUnitsJson.edges;

    units.forEach(unit => {
      createPage({
        path: `${unit.node.lang}/database/unit/${unit.node.id}`,
        component: unitTemplate,
        context: {
          locale: unit.node.lang,
          id: unit.node.id
        }
      });
    });

    posts.forEach((post, index) => {
      const previous =
        index === posts.length - 1 ? null : posts[index + 1].node;
      const next = index === 0 ? null : posts[index - 1].node;
      const slug = post.node.fields.slug;
      const title = post.node.frontmatter.title;
      const locale = post.node.fields.locale;
      const parentDir = post.node.fields.parentDir;
      const path = `${locale}${post.node.fields.slug}`;
      createPage({
        path: path,
        component: blogTemplate,
        context: {
          locale,
          title,
          slug: slug,
          parentDir,
          previous,
          next
        }
      });
    });
    return null;
  });
};

// Adding this, so Gatsby-mdx could import js components from src
// Issue number #176 on github
exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    resolve: {
      modules: [path.resolve(__dirname, "src"), "node_modules"]
    }
  });
};
