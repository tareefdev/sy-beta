// custom typefaces
import "typeface-tajawal";
import { includes, without } from "lodash";

const shouldUpdateScroll = (args, page) => {
  // A workaound for several bugs:
  // On desktop we want to preserve scrolling position after opnning a modal.
  // But this conflicts with TOC see Gatsby.js #1719 so we return false here.
  const windowWidth = window.innerWidth;
  const isDataarchivePage = includes(window.location.pathname, "data-archive");
  const pathLong = without(window.location.pathname.split("/"), "").length; // pages which have TOC
  const isBlogPost = pathLong > 2; // return false for pages contains TOC
  const isMobile = windowWidth < 750; // only on Desktop
  return isDataarchivePage ? false : isBlogPost ? false : true;
};

const onInitialClientRender = () => {
  window.___MNEMONIC_INITIAL_RENDER_COMPLETE = true;
};

export { onInitialClientRender, shouldUpdateScroll };
export { wrapPageElement } from "./src/context/locale-context";
