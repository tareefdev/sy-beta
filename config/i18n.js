module.exports = {
  en: {
    default: true,
    path: `en`,
    locale: `en-US`,
    siteLanguage: `en`,
    ogLanguage: `en_US`
  },
  ar: {
    default: false,
    path: `ar`,
    locale: `ar-AR`,
    siteLanguage: `ar`,
    ogLanguage: `ar_AR`
  }
};
